import re, os.path, random, shutil
from ascii import HANGMAN_PHOTOS, HANGMAN_OPEN_ASCII

continue_play = 1 #will be used per user input to signal the main loop whether to continue running
def main():
	"""Main game (round) function with turn loop.
	:return: None
	"""
	MAX_TRIES = 6
	num_of_tries = 0 #incremented with each wrong (valid) attempt
	old_letters_guessed = [] #list of all valid letters attempted during the round
	winlose = 0 #used to signal the turn loop if the round is over or continue running
	global continue_play
	file_path, index_num = loading_screen(MAX_TRIES)
	if os.path.isfile(file_path):
		secret_word = choose_word(file_path, index_num)
	else:
		secret_word = choose_word(r"words.txt", index_num) #default words list file
	while winlose == 0:
		#print(secret_word) #enable for cheating
		print("\nThe game word is:\n" + show_hidden_word(secret_word, old_letters_guessed) + "\n") #show the word in spaced characters, redacted if not yet guessed
		letter_guessed = input("Enter a letter: ").lower()
		if try_update_letter_guessed(letter_guessed, old_letters_guessed): #for valid attempt, adds this turn's letter_guessed to old_letters_guessed
			print(show_hidden_word(secret_word, old_letters_guessed))
			if letter_guessed not in secret_word:
				num_of_tries += 1
				print("\n\nThis guess is WRONG.\n")
				print("Number of wrong guesses left:", MAX_TRIES - num_of_tries)
				print_hangman(num_of_tries)
			else:
				print("\n\nGood guess.\n")
		if check_win(secret_word, old_letters_guessed): #ending turn and checking win conditions before next turn
			winlose = 2
			print("Congratulations. You are the winner.")
		else:
			if num_of_tries == MAX_TRIES:
				winlose = 1
				print("Game over. Thank you for playing.")
		input("Press Enter to continue")
	continue_play = input("\nEnter \"Y\" to play again, or any other input to quit:\n").lower() == "y"

def loading_screen(MAX_TRIES):
	"""Displays on start, get game input.
	:param MAX_TRIES: random value previously assigned
	:type MAX_TRIES: int
	:return: Path of words file
	:rtype: str
	:return: Index number for word in file_path
	:rtype: str
	"""
	print (f"""{HANGMAN_OPEN_ASCII}\nThis time, you are allowed {MAX_TRIES} failed attempts.""")
	file_path = input("Enter words file path: ")
	index_num = input("Enter index number: ")
	return file_path, index_num

def print_hangman(num_of_tries):
	"""Prints progressing picture after failed attempt.
	:param num_of_tries: failed attempts
	:type num_of_tries: int
	:return: None
	"""	
	print("\nYour current hanging status:\n" + HANGMAN_PHOTOS[num_of_tries + 1] + "\n")

def old_letters_formatter(old_letters_to_format):
	"""returns old_letters_guessed formatted per instructions for error message.
	:param old_letters_to_format: old_letters_guessed
	:type old_letters_to_format: list
	:return: old_letters_guessed formatted per instructions for error message
	:rtype: str
	"""	
	old_letters_to_format.sort()
	formatted_old_letters = " -> ".join(old_letters_to_format)
	return formatted_old_letters
	
def try_update_letter_guessed(letter_guessed, old_letters_guessed):
	"""If input is valid, add to guessed letters and True.
	:param letter_guessed: current letter attempted
	:param old_letters_guessed: letters previously guessed
	:type letter_guessed: str
	:type old_letters_guessed: list
	:return: T/F
	:rtype: bool
	"""	
	is_valid = check_valid_input(letter_guessed)
	is_already = letter_guessed in old_letters_guessed
	if not is_valid or is_already:
		print("X\nInvalid input.", f"\n{old_letters_formatter(old_letters_guessed)}")
		return False
	else:
		old_letters_guessed.append(letter_guessed)
		return True

def check_valid_input(letter_guessed):
	"""Check if input is a single alphabet char.
	:param letter_guessed: current letter attempted
	:type base: str
	:return: T/F
	:rtype: bool
	"""	
	rex_pattern = re.compile("[a-z]+") #create regex pattern
	is_multiple = len(letter_guessed) > 1
	is_letter = rex_pattern.fullmatch(letter_guessed) #use regex pattern, return T/F 
	if is_multiple or is_letter is None:
		return False
	else:
		return True
		
def show_hidden_word(secret_word, old_letters_guessed):
	"""Prints the hidden word with redaction of non-guessed letters.
	:param secret_word: game word chosen on start
	:param old_letters_guessed: letters previously guessed
	:type secret_word: str
	:type old_letters_guessed: list
	:return: hidden word with redaction of non-guessed letters
	:rtype: str
	"""
	secret_list = list(secret_word)
	filtered_list = []
	for secret_letter in secret_list:
		if secret_letter in old_letters_guessed:
			filtered_list.append(f"{secret_letter}")
		else:
			filtered_list.append("_")

	filtered_word = " ".join(filtered_list)
	return filtered_word

def check_win(secret_word, old_letters_guessed):
	"""Runs after each turn to see if player has won or game over.
	:param secret_word: game word chosen on start
	:param old_letters_guessed: letters previously guessed
	:type secret_word: str
	:type old_letters_guessed: list
	:return: T/F
	:rtype: bool
	"""	
	secret_word_letters = list(secret_word)
	all_guessed = True
	for letter in secret_word_letters:
		if letter in old_letters_guessed:
			continue
		else:
			all_guessed = False
	if all_guessed:
		return True
	else:
		return False

def choose_word(file_path, index):
	"""On game start, after receiving list of words, choose a word from the list according to index number input by user.  
	:param file_path: path received from user to file of words
	:param index: number received from user to select which word will be used as secret_word.
	:type file_path: str
	:type index: int
	:return: The word requested by user
	:rtype: str
	"""	
	new_list = []
	unq_words = 0
	with open(file_path, "r") as txt:
		list_of_words = txt.read().split(" ")
		for word in list_of_words:
			if word not in new_list:
				unq_words += 1
				new_list.append(word)
	
		if not index.isnumeric():
			index = random.randint(1,256)
		word_in_index = list_of_words[(int(index) % len(list_of_words)) - 1]
		return (word_in_index)

while continue_play:
	main()
print("Goodbye.")


