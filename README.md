How to use: 
1. Copy words.txt to your hard drive 
2. Run hangman.py 
3. Enter the location of the file containing your list of words and the index number of the word to be guessed. 


Hangman game. Guess the letters.

How to play:

The number of allowed wrong guesses is shown to you at the beginning of the game.

See the redacted characters of the word and enter a letter you believe may be hidden therein.

If you guessed correctly, you will see the redacted word with all the letters you have guessed so far revealed.

If you guess all letters to reveal the word without exhausting your allowed number of wrong guesses, you will be decalred the winner.

If you exhaust your allowed number of wrong guesses before guessing all the letters correctly, you will be decalred the loser.

Good luck.
